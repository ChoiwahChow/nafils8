# Nafils8

This repository contains the code to find all non-isomorphic non-associative finite invertible loops (nafils) of order 8.  It can easily be adapted to find nafils of different orders.

## Overview of the Process

Mace4 is used to generate all candidates of invertible loops of order 8.  With least number heuristics (lnh) turned on, many isomorphic models are not generated. This greatly reduces the number of models to process in the next stage.  Next, we calculate the invariant vector of each model.  Models with different invariant vectors cannot be isomorphic.  Thus, we only need to check for isomorphisms between models having the same invariant vector.  This is done using the functions provided by the GAP's loops package.  The models produced in this step are all the non-isomorphic invertible loops.  Finally, we use the loops package's IntoGroup function to filter out those associative invertible loops, which are groups by definition.  The final outputs are therefore nafils.

### Special Note

It is certainly possible to instruct Mace4 to exclude associative models:

```text
(a * b) * c != a * (b * c).   % non-associative
```
However, with lnh is turned on, this clause would cause Mace4 to generate a lot more candidate models instead of fewer models.  Since there are very few associative invertible loops (groups), so it is a lot more efficient to filter them out in later steps.

## Project Structure

The project directory is nafils8. Source code files are in nafils8/src/nafils8:
1. mace_to_gap.py: converts Mace4 outputs to GAP's multiplication table format. One line per model.

2. nafils.py: the main script to take the outputs of mace_to_gap.py to calculate the invariant vectors of all models, and kicks off parallel GAP sessions to filter out isomorphic models and models that are groups.

3. non_iso_loops.g: GAP script to filter out isomorphic models and models that are groups.

The inputs are in the nafils8/inputs directory:
1. mace_nafils_8.in: Mace4 input file

The outputs are deposited in the nafils8/outputs directory.  The Mace4 output file is nafils8/outputs/nafils_8.out.

There are other sub-directories under the outputs directory. The outputs from mace_to_gap.py are deposited in nafils8/outputs/nafils8_gap.
The outputs from nafils.py and the final output file are in nafils8/outputs/nafils8_py.

## Steps to Produce Nafils of Order 8

Make output directories if they don't already exist:
```text
	mkdir outputs/nafils8_gap
	mkdir outputs/nafils8_py
```

- Generate all invertible loops of order 8 (3,004 seconds).
	At the project top dir:
	
```text
	mace4 -f inputs/mace_nafils_8.in > outputs/nafils_8.out
```
- Extract the binary multiplication tables from the outputs and convert to gap format (1,200 seconds).
	At the project top dir:
	
```text
	date; cat outputs/nafils_8.out  | src/nafils8/mace_to_gap.py; date
```

- Use Python and GAP to throw out isomorphic models and groups (85,185 seconds, almost 24 hours).
	At the project top dir:
```text
    date; src/nafils8/nafils.py --outdir outputs/nafils8_py --outfile test.g; date
```
    
- Put the outputs from all threads together.
	At the outputs directory, outputs/nafils8_py:
	
```text
    cat models.g.[0-7] > nafils8.g
```

## Results

Using a 32G RAM, 6-core Intel i7-9850 CPU (2.6GHz x 12) computer running Ubuntu Linux, the whole process of generating all nafils of order 8 took less than 25 hours and found all 4,177,212 non-isomorphic models.

Mace4 generated 73,743,872 candidate models (with isomorphisms).  The Python script generated 4,177,161 distinct invariant vectors (hash keys).
We see that the algorithm for generating hash keys is very effectively - out of over 4 million non-isomorphic models, it only hashes 51 of them to filled buckets.

A copy of the nafils of order 8 is in nafils8/outputs/nafils8_py/nafils8.zip.
