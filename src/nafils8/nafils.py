#!/usr/bin/python3

"""
This script takes a list of models in gap's format, calculate the distributions of the
domain elements as a hash value.  Models with different hash values cannot be isomorphic. This is a way
to hash the models into different buckets to quickly filter out isomorphic models.

The hash keys are based on the invariants given in the iso.gi in the loops package in GAP. It is
modified to handle loops only.

cells are lists of rows (lists) of the function table
columns are lists of columns (lists) of the function table


Customizations that can easily be made below:

Number of processes to run in parallel:
max_threads = 8

List of input files of modles in gap's format:
infile_list = [f"nafils8_{x}.g" for x in range(0, 15)]

Number of models to run in each gap session to filter out isomorphic models:
if models_count > 3000 or count >= len(all_models):
 

"""

import sys
import os
import time
from datetime import datetime
import subprocess
import collections
import itertools
import threading

import argparse


default_out_dir = "./outputs/nafils8_py"
default_outfilename = "models.g"


def distribution(data_list):
    if not data_list:
        return tuple()

    freq = collections.Counter(data_list)
    dist = list()
    for k in sorted(freq.keys()):
        dist.append((k, freq[k]))
    return tuple(dist)


def has_identity(cells, columns):
    for x in range(len(cells)):
        if cells[x][x] != x:
            continue
        row = cells[x]
        if all(row[i] == i for i in range(len(row))):
            c = columns[x]
            if all(c[i] == i for i in range(len(c))):
                return x
    return None


def cycle_length(row, pos):
    el = row[pos]
    len_cycle = 1
    next_el = row[el]
    row[el] = -1
    while next_el != el:
        len_cycle += 1
        k = row[next_el]
        row[next_el] = -1
        next_el = k
    return len_cycle
    

def find_cycle_lengths(in_row):
    cycles = []
    row = [x for x in in_row]
    for x in range(len(row)):
        if x == row[x] or row[x] == -1:
            row[x] = -1
        else:
            cycles.append(cycle_length(row, x))
    return distribution(cycles)


def CycleStructurePerm(l):
    return find_cycle_lengths(l)


"""
    # invariant 1
    # to distinguish the 3 cases
    if (not IsLoop( Q ) )
        then case := 1;
    elif (not IsPowerAssociative( Q ))
        then case := 2;
    else
        case := 3;
    fi;
    for i in [1..n] do I[i][1] := case; od;
"""


def invariant_1(cells, columns, keys):
    e = has_identity(cells, columns)
    case = 2
    for row in range(len(keys)):
        keys[row][0] = case
    keys[-1][0] = [case] * 9
    return e
    

"""
    # invariant 2
    # for given x, cycle structure of L_x, R_x
    for i in [1..n] do
        I[i][2] := [
            CycleStructurePerm( PermList( List( [1..n], j -> T[i][j]) ) ),
            CycleStructurePerm( PermList( List( [1..n], j -> T[j][i]) ) )
        ];
    od;
"""

def invariant_2(cells, columns, keys):
    for el in range(len(cells)):
        keys[el][1] = [CycleStructurePerm(cells[el]), CycleStructurePerm(columns[el])]
    keys[-1][0][1] = (distribution([keys[x][1][0] for x in range(len(keys)) if keys[x][1][0]]),
                      distribution([keys[x][1][1] for x in range(len(keys)) if keys[x][1][1]]))


"""
    # invariant 3
    if case = 1 then # am I an idempotent?
        for i in [1..n] do I[i][3] := T[i][i]=i; od;
    elif case = 2 then # am I an involution?
        for i in [1..n] do I[i][3] := T[i][i]=1; od;
    else # what's my order?
        for i in [1..n] do I[i][3] := Order( Elements( Q )[i] ); od;
    fi;
"""
# final key string: number of idempotent, or number of involutions, or distributions of order (1_1_2_4 1 order 1, 2 order 4 etc).

def invariant_3(cells, keys, e):
    counter = 0
    for x in range(len(cells)):
        keys[x][2] = cells[x][x] == e
        if keys[x][2]:
            counter += 1
    keys[-1][0][2] = counter
        

"""
    # invariant 4
    if case <> 3 then # how many times am I a square ?
        for i in [1..n] do j := T[i][i]; I[j][4] := I[j][4] + 1; od;
    else # how many times am I a square, third power, fourth power?
        for i in [1..n] do I[i][4] := [0,0,0]; od;
        for i in [1..n] do
            j := T[i][i]; I[j][4][1] := I[j][4][1] + 1;
            j := T[i][j]; I[j][4][2] := I[j][4][2] + 1;
            j := T[i][j]; I[j][4][3] := I[j][4][3] + 1;
        od;
    fi;
"""    

def invariant_4(cells, keys):
    for el in range(len(keys)):
        keys[el][3] = 0
    for el in range(len(keys)):
        keys[cells[el][el]][3] += 1
    row = [keys[x][3] for x in range(len(keys)) if keys[x][3] > 0]
    keys[-1][0][3] = distribution(row)


"""
    # invariant 5
    if case <> 3 then #  with how many elements do I commute?
        for i in [1..n] do
            I[i][5] := Length( Filtered( [1..n], j -> T[i][j] = T[j][i] ) );
        od;
    else # with how many elements of given order do I commute?
        ebo := List( [1..n], i -> Filtered( [1..n], j -> I[j][3]=i ) ); # elements by order. PROG: must point to order invariant
        ebo := Filtered( ebo, x -> not IsEmpty( x ) );
        for i in [1..n] do
            I[i][5] := List( ebo, J -> Length( Filtered( J, j -> T[ i ][ j ] = T[ j ][ i ] ) ) );
        od;
    fi;
"""

def invariant_5(cells, keys):
    for el in range(len(keys)):
        keys[el][4] = sum([1 if cells[el][col] == cells[col][el] else 0 for col in range(len(keys))])   # to do: improve with itertools izip
    keys[-1][0][4] = distribution([keys[x][4] for x in range(len(keys)) if keys[x][4] > 0])


"""
    # invariant 6
    # is it true that (x*x)*x = x*(x*x)?
    for i in [1..n] do
        I[i][6] :=  T[T[i][i]][i] = T[i][T[i][i]];
    od;
"""

def invariant_6(cells, keys):
    for el in range(len(keys)):
        el2 = cells[el][el]
        keys[el][5] = cells[el2][el] == cells[el][el2]
    keys[-1][0][5] = sum([1 for x in range(len(keys)) if keys[x][5]])
    

"""
    # invariant 7
    if case <> 3 then # for how many elements y is (x*x)*y = x*(x*y)?
        for i in [1..n] do
            I[i][7] := Length( Filtered( [1..n], j -> T[T[i][i]][j] = T[i][T[i][j]] ) );
        od;
    else # for how many elements y of given order is (x*x)*y=x*(x*y)
        for i in [1..n] do
            I[i][7] := List( ebo, J -> Length( Filtered( J, j -> T[T[i][i]][j] = T[i][T[j][i]] ) ) );
        od;
    fi;
"""


def invariant_7(cells, keys):
    for el in range(len(keys)):
        el2 = cells[el][el]
        keys[el][6] = sum([1 for y in range(len(keys)) if cells[el2][y] == cells[el][cells[el][y]]])
    keys[-1][0][6] = distribution([keys[x][6] for x in range(len(keys)) if keys[x][6] > 0])


"""
    # invariants 8 and 9 (these take longer)
    if case <> 3 then # with how many pairs of elements do I associate in the first, second position?
        for i in [1..n] do
            for j in [1..n] do for k in [1..n] do
                if T[i][T[j][k]] = T[T[i][j]][k] then I[i][8] := I[i][8] + 1; fi;
                if T[j][T[i][k]] = T[T[j][i]][k] then I[i][9] := I[i][9] + 1; fi;
            od; od;
        od;
    else # for how many pairs of elements of given orders do I associate in the first, second position?
        for i in [1..n] do
            I[i][8] := []; I[i][9] := [];
            for js in ebo do for ks in ebo do
                count1 := 0; count2 := 0;
                for j in js do for k in ks do
                    if T[i][T[j][k]] = T[T[i][j]][k] then count1 := count1 + 1; fi;
                    if T[j][T[i][k]] = T[T[j][i]][k] then count2 := count2 + 1; fi;
                od; od;
                Add( I[i][8], count1 ); Add( I[i][9], count2 );
            od; od;
        od;   
    fi;
"""


def invariant_8_9(cells, keys):
    for el in range(len(keys)):
        keys[el][7] = 0
        keys[el][8] = 0
        for j in range(len(keys)):
            for k in range(len(keys)):
                if cells[el][cells[j][k]] == cells[cells[el][j]][k]:
                    keys[el][7] += 1
                if cells[j][cells[el][k]] == cells[cells[j][el]][k]:
                    keys[el][8] += 1
    keys[-1][0][7] = distribution([keys[x][7] for x in range(len(keys)) if keys[x][7] > 0])
    keys[-1][0][8] = distribution([keys[x][8] for x in range(len(keys)) if keys[x][8] > 0])


def calc_discriminator(cells, domain_size):
    keys = list()
    for _ in range(domain_size):
        keys.append([0]*9)
    columns = [[cells[row][col] for row in range(domain_size)] for col in range(domain_size)]
    e = invariant_1(cells, columns, keys)
    invariant_2(cells, columns, keys)
    invariant_3(cells, keys, e)
    invariant_4(cells, keys)
    invariant_5(cells, keys)
    invariant_6(cells, keys)
    invariant_7(cells, keys),
    invariant_8_9(cells, keys)
    
    return tuple([keys[-1][0][0], keys[-1][0][1], keys[-1][0][2], keys[-1][0][3], keys[-1][0][4],
                  keys[-1][0][5], keys[-1][0][6], keys[-1][0][7], keys[-1][0][8]])
    
    
def classify_model(all_models, table, model, domain_size):
    """cells is a list of integers 
       model is a list of strings (lines in input)
    """
    key = calc_discriminator(table, domain_size)
    #all_models[key].append("".join(model))
    all_models[key].append(model)


def thread_available(thread_count, thread_slots):
    for x in range(thread_count):
        if thread_slots[x] == 0:
            return x
    return -1


def all_done(thread_slots):
    for x in range(len(thread_slots)):
        if thread_slots[x] != 0:
            return False
    return True
    

max_threads = 8
thread_slots = [0] * max_threads


def reduce_non_iso(slot, all_models, key, model_list_str, full, outfp=""):
    try:
        inlist = f'models := [{model_list_str}];;\nprocess_iso(models, {full});;'
        temp_in = f"{outfp}.{slot}.in"
        with (open(temp_in, "w")) as infp:
            infp.write(inlist)
        cp = subprocess.run(f"(cat src/nafils8/non_iso_loops.g; echo '\nRead(\"{temp_in}\");;\n') | gap >> /dev/null", capture_output=True, text=True, check=True, shell=True)
        if cp.returncode == 0:
            if len(cp.stderr) == 0:
                all_models[key] = []
            else:
                models_list = cp.stderr.replace("\\\n", "").split(";")
                if full == 1:
                    with (open(f"{outfp}.{slot}", "a")) as fp:
                        for m in models_list:
                            fp.write(f"{m}\n")
                else:
                    all_models[key] = models_list
        else:
            print(f"\ngap failed *************\n\n", file=sys.stderr)
    finally:
        thread_slots[slot] = 0


def reduce_one_non_iso(all_models):
    counter = 0
    for key, val in all_models.items():
        if len(val) > 200:
            counter += 1
            slot_id = thread_available(max_threads, thread_slots)
            while slot_id < 0:
                time.sleep(0.5)
                slot_id = thread_available(max_threads, thread_slots)
            thread_slots[slot_id] = threading.Thread(target=reduce_non_iso, args=(slot_id, all_models, key, f'[{",".join(val)}]', 0))
            thread_slots[slot_id].start()
                
    while(not all_done(thread_slots)):
        time.sleep(0.5)
    return counter


def reduce_all_non_iso(all_models, master_out_file_path):
    model_list = list()
    models_count = 0
    count = 0
    for key, val in all_models.items():
        count += 1
        if count % 200000 == 0:
            print(f"{datetime.now()}** doing {count}-th key")
        model_list.append(f'[{",".join(val)}]')
        models_count += len(val)
        all_models[key] = list()
        if models_count > 3000 or count >= len(all_models):
            slot_id = thread_available(max_threads, thread_slots)
            while slot_id < 0:
                time.sleep(0.5)
                slot_id = thread_available(max_threads, thread_slots)
            thread_slots[slot_id] = threading.Thread(target=reduce_non_iso, args=(slot_id, all_models, key, f'{",".join(model_list)}', 1, master_out_file_path))
            thread_slots[slot_id].start()
            model_list = list()
            models_count = 0
    
    while(not all_done(thread_slots)):
        time.sleep(0.5)
        

def main():
    """
    Each line in the input file represents a model in gap's multiplication format.  They are converted to Mace format to calculate
    the invariant, but the original gap format is stored in the bucket corresponding to the invariant.
    After invariants of all models are calculated and hashed, the hash table is traversed. Each bucket of models are passed separately
    to gap to remove isomorphic models. Since models with different hash keys are not isomorphic, processing each bucket separately
    would be much faster and still gives the correct results.  Since bringing up a gap session and reading in data has high overheads,
    we put multiple buckets in each session to reduce the number of sessions.  In the gap code, each bucket is processed separately
    to maintain efficiency.  Multiple gap sessions are run in parallel, and the results are written out in separate files.
    These files are concatenated together to give the final results outside this script.
    """
    parser = argparse.ArgumentParser(prog="nafils.py",
                                     usage='%(prog)s [options] --outdir output-dir --outfile output-file',
                                     description='filter out isomorphic models')
    parser.add_argument('--outdir', type=str, required=False, help='outdir - the full file path of output dir')
    parser.add_argument('--outfile', type=str, required=False, help='outfile - the file name of non-isomorphic models file')

    args = parser.parse_args()

    if args.outdir:
        outdir = args.outdir
    else:
        outdir = default_top_dir
    os.makedirs(outdir, exist_ok = True)

    if args.outfile:
        outfile = args.outfile
    else:
        outfile = default_outfilename
        
    master_out_file_path = os.path.join(outdir, outfile)

    all_models = collections.defaultdict(list)

    domain_size = 0
    input_counter = 0
    file_counter = 0;
    
    infile_list = [f"nafils8_{x}.g" for x in range(0, 15)]

    for infile in infile_list:
        infile = os.path.join('outputs', 'nafils8_gap', infile)
        file_counter += 1
        with open(infile) as fp:
            while True:
                line = fp.readline().rstrip()
                if not line:
                    break
                if not line.startswith('[['):
                    continue
                input_counter += 1
                rows = line[2:-2].split("],[")   # starts with [[, ends with ]]
                py_cells = list()
                for row in rows:
                    py_row = [int(x)-1 for x in row.split(',')]
                    py_cells.append(py_row)
                if domain_size == 0:
                    domain_size = len(py_cells)
                classify_model(all_models, py_cells, line, domain_size)   # no new line char, has comma
        print(f"Loop: processed {infile}.  Cumulative {input_counter} models, {len(all_models.keys())} groups", file=sys.stderr)
        #reduce_count = reduce_one_non_iso(all_models)
        #print(f"{datetime.now()}** Intermediate reduced key count: {reduce_count}\n")


    print(f"{datetime.now()}**: processed {input_counter} models, {len(all_models.keys())} groups", file=sys.stderr)
    print(f"Final gap out file {master_out_file_path}\n")
    reduce_all_non_iso(all_models, master_out_file_path)

    print(f"{datetime.now()}** Completed: processed {input_counter} models", file=sys.stderr)


if __name__== "__main__":
    main()
    





