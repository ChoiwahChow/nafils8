#!/usr/bin/python3

"""
This script reads Mace4 outputs from stdin and writes the multiplication table out in 
gap standard format, i.e start with 1 instead of 0.  Each line in the output is 
one multiplication table, which is assumed to be a 2-ary function, *.
It does not include constants or functions other than the * function.
This is to write out the multiplication table fast.

It is set up to run in the top dir of the project to go to the out_dir.
"""

import sys
import os

domain_size = 8
max_models_in_file = 5000000   # 5 millions per file for convenience
out_dir = "outputs/nafils8_gap"
out_base_name = "nafils8"

function_name = "        function(*(_,_), ["


def main():
    model_count = 0
    file_counter = 0
    out_file = os.path.join(out_dir, f"{out_base_name}_{file_counter}.g")
    fp = open(out_file, "w")
    file_counter += 1
    in_model = False
    model = list()
    for line in sys.stdin:
        if len(line) < 2:
            continue
        if line.startswith(function_name):
            in_model = True
        elif in_model and "])" in line:
            in_model = False
            line = line.split("])")[0]
            model.append([int(x)+1 for x in line.strip().split(',')])
            fp.write(f"{str(model).replace(' ', '')}\n")
            model_count += 1
            if model_count == max_models_in_file:
                fp.close()
                out_file = os.path.join(out_dir, f"{out_base_name}_{file_counter}.g")
                fp = open(out_file, "w")
                file_counter += 1
                model_count = 0
            model = list()
        elif in_model:
            model.append([int(x)+1 for x in line.strip().rstrip(',').split(',')])

    fp.close()

if __name__== "__main__":
    main()
    





