
LoadPackage("loops");;

# Notes
# LoopByCayleyTable works only on normalized latin square.

sep := ";";

process_iso2 := function(models, full)
	local all_loops, non_iso, x, non_assoc;

	all_loops := List(models, p->LoopByCayleyTable(p));
	
	if Length(all_loops) > 1 then
		non_iso := LoopsUpToIsomorphism(all_loops);
	else
		non_iso := all_loops;
	fi;

	if full = 1 then
		non_assoc := [];
		for x in non_iso do
			if IntoGroup(x) = fail then Add(non_assoc, x); fi;
		od;
		non_iso := non_assoc;
		if Length(non_iso) = 0 then return []; fi;
	fi;

	return non_iso;
end;

process_iso := function(models, full)
	local x, all_res, non_iso_str_list, final_non_iso_list;

	all_res := [];
	for x in models do
		Append(all_res, process_iso2(x, full));
	od;
	non_iso_str_list := List(all_res, p->String(MultiplicationTable(p)));
	final_non_iso_list := JoinStringsWithSeparator(non_iso_str_list, sep);
	RemoveCharacters(final_non_iso_list, " \\\n");
	PrintTo("*errout*", final_non_iso_list);
end;



